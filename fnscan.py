#!/usr/bin/env python3

import os
import argparse
import itertools
import string
import time
import sys
from unidecode import unidecode
import warnings

################################################################################
# Configuration

allowedChars = set(string.ascii_letters + string.digits + ".@,-$_#[] ~+()'=%!{}")
warnings.simplefilter("ignore")

################################################################################
# Command line parsing

p = argparse.ArgumentParser()
p.add_argument("dir", help="directory to inspect")
p.add_argument("-r", "--recursive", help="check subdirectories recursively", action="store_true")
p.add_argument("-p", "--progress",  help="regularly print progress", action="store_true")
p.add_argument("-f", "--fix", help="try to fix simple problems", action="store_true")
p.add_argument("-y", "--yes", help="answer yes to all questions", action="store_true")
p.add_argument("-m", "--max-errors", help="terminate after MAX_ERRORS", type=int)

args = p.parse_args()
nErrors = 0
currentDirPrinted = False

################################################################################
# Functions

# Returns True if maximum number of errors have been reached
def maxErrorsReached():
    global nErrors
    return args.max_errors is not None and nErrors >= args.max_errors

# Used to avoid recursing into files that we don't bother about
def isRepositoryRoot(root):
    return os.path.isdir(root + "/.git") \
        or os.path.isdir(root + "/.hg")

# Print an error message
def err(root, msg):
    global nErrors, args, currentDirPrinted
    if not currentDirPrinted:
        print("Errors in " + root)
        currentDirPrinted = True
    nErrors += 1
    try:
        print("    " + msg)
    except UnicodeEncodeError:
        print("    " + unidecode(msg) + " [contains unprintable characters]")
    if maxErrorsReached():
        terminate()

# Check that all given names are unique
def checkUniqness(root, names):
    seen = {}
    for name in names:
        lower = name.lower()
        if lower in seen:
            existing = seen[lower]
            err(root, "Both \"" + existing + "\" and \"" + name + "\" found")
        seen[lower] = name

# Checks that the given file name only contains legal characters
def allCharactersLegal(name):
    global allowedChars
    return set(name) <= allowedChars

# Attempt to clean up a file name
def sanitizeFilename(name):
    name = unidecode(name)
    return name.replace("å", "a") \
               .replace("ä", "a") \
               .replace("ö", "o") \
               .replace(":", "_") \
               .replace(";", "_") \
               .replace("?", "_") \
               .replace("`", "'") \
               .replace("&", "and")

# Check characters in file names
def checkLegalCharacters(root, names):
    global args
    for name in names:
        if not allCharactersLegal(name):
            err(root, "Illegal character in \"" + name + "\"")
            newSuggestion = sanitizeFilename(name)
            if args.fix and allCharactersLegal(newSuggestion):
                if args.yes or input("    Rename to \"" + newSuggestion + "\"? [Y/n] ") in "Yy":
                    print("        Renaming file to \"" + newSuggestion + "\"...")
                    os.rename(root + "/" + name, root + "/" + newSuggestion)

# Returns a (root, dirs, files) generator for desired dirs
def allDirs():
    global args
    dirGen = os.walk(args.dir)
    return dirGen if args.recursive else itertools.islice(dirGen, 1)

# Count files (used to determine progress)
def countFiles():
    n = 0
    for _, dirs, files in allDirs():
        n += len(files) + len(dirs)
    return n

# Actual file inspection
def inspectFiles(fileCount):
    global args, currentDirPrinted
    nInspected = 0
    t = 0
    for root, dirs, files in allDirs():
        currentDirPrinted = False
        if args.progress and time.time() - t > 5:
            print("{:.1%} inspected...".format(nInspected / fileCount), file=sys.stderr)
            t = time.time()
        allNames = dirs + files
        checkUniqness(root, allNames)
        checkLegalCharacters(root, allNames)
        nInspected += len(dirs) + len(files)

        # Don't recurse into repositories
        dirs[:] = [d for d in dirs if not isRepositoryRoot(root + "/" + d)]

# Print number of errors and exit
def terminate():
    if nErrors == 0:
        print("No errors found.")
    elif nErrors == 1:
        print("1 error found.")
    else:
        print("{} errors found.".format(nErrors))
    sys.exit(0 if nErrors == 0 else 1)

################################################################################
# Script entry point

if args.progress:
    print("Counting files...")
    fileCount = countFiles()
    print("Found {:d} files".format(fileCount))
else:
    fileCount = None

inspectFiles(fileCount)

terminate()
